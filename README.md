# Spalte

A punched-through font designed in a sunny day in Graz.

![Font preview](https://gitlab.com/typebits/font-spalte/-/jobs/artifacts/master/raw/Spalte-Regular.png?job=build-font)
  
[See it in action](https://typebits.gitlab.io/font-spalte/)

Download formats:

* [TrueType (.ttf)](https://gitlab.com/typebits/font-spalte/-/jobs/artifacts/master/raw/Spalte-Regular.ttf?job=build-font)
* [OpenType (.otf)](https://gitlab.com/typebits/font-spalte/-/jobs/artifacts/master/raw/Spalte-Regular.otf?job=build-font)
* [FontForge (.sfd)](https://gitlab.com/typebits/font-spalte/-/jobs/artifacts/master/raw/Spalte-Regular.sfd?job=build-font)

# Authors

* [Ana Isabel Carvalho](http://manufacturaindependente.org)
* [Jogi Hofmüller](https://thesix.mur.at)
* [JoaK](http://nospace.at)
* [Ricardo Lafuente](http://manufacturaindependente.org)

# License

This font is made available under the [Open Font License](https://scripts.sil.org/OFL).
