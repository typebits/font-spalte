FONT_NAME = Spalte
BITMAPFONTBUILDER_SCRIPT = ../bitmapfontbuilder/bmfb.py

default: build

# simple local build
build:
	python $(BITMAPFONTBUILDER_SCRIPT) $(FONT_NAME)-Regular.json
	mv generated/* . -f
	. `pwd`/.env/bin/activate; fib convert $(FONT_NAME)-Regular.sfd --ttf
	fontimage $(FONT_NAME)-Regular.otf

# for use within gitlab's CI environment
ci:
	python bitmapfontbuilder/bmfb.py $(FONT_NAME)-Regular.json
	mv generated/* . -f
	fib convert $(FONT_NAME)-Regular.sfd --ttf
	fontimage $(FONT_NAME)-Regular.otf

install:
	virtualenv .env --system-site-packages
	. `pwd`/.env/bin/activate; pip install git+https://gitlab.com/foundry-in-a-box/fib.git
