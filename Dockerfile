FROM debian
RUN rm -rf /var/lib/apt/lists/* \
           /etc/apt/sources.list.d/cuda.list \
           /etc/apt/sources.list.d/nvidia-ml.list

RUN apt update

RUN DEBIAN_FRONTEND=noninteractive apt install -y --no-install-recommends \
    python \
    python-pip \
    fontforge \
    python-fontforge \
    make \
    git

# RUN python -m pip --no-cache-dir install --upgrade
RUN pip --no-cache-dir install --upgrade pip
RUN python -m pip --no-cache-dir install --upgrade setuptools
RUN python -m pip --no-cache-dir install --upgrade git+https://gitlab.com/foundry-in-a-box/fib.git
RUN python -m pip --no-cache-dir install --upgrade \
    click \
    zenlog

RUN git clone https://gitlab.com/typebits/bitmapfontbuilder
WORKDIR "/typebits"
CMD ["make", "--makefile=Makefile.docker"]
